import 'package:flutter/material.dart';

class Imgcard2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Card(
            shadowColor: Colors.black,
            color: Colors.white,
            child: Image(
              image: AssetImage("assets/Dario.jpg"),
            )));
  }
}
