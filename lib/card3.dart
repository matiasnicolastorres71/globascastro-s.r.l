import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/src/pages/pagina3.dart';

class Card3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        shadowColor: Colors.black,
        color: Colors.white,
        child: ListTile(
          leading: Icon(CupertinoIcons.info),
          title: Text(
            "¿Quienes Somos?",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => Pagina3()));
          },
        ),
      ),
    );
  }
}
