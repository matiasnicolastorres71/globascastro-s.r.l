import 'package:flutter/material.dart';

class Navegacion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var bottomNavigationBar = BottomNavigationBar(
        backgroundColor: Colors.blueGrey[50],
        elevation: 5,
        currentIndex: 0,
        items: [
          // ignore: deprecated_member_use
          BottomNavigationBarItem(
            // ignore: deprecated_member_use
            icon: Icon(Icons.search),
            // ignore: deprecated_member_use
            title: Text("buscar"),
          ),
          // ignore: deprecated_member_use
          BottomNavigationBarItem(
            // ignore: deprecated_member_use
            icon: Icon(Icons.phone),
            // ignore: deprecated_member_use
            title: Text("Contacto"),
          ),
        ]);
    return bottomNavigationBar;
  }
}
