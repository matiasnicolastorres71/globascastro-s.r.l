import 'package:flutter/material.dart';

import 'pagina1config.dart';

class Cardconfig1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        shadowColor: Colors.black87,
        color: Colors.white,
        child: ListTile(
            title: Text(
              "OPCION 1",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Pagina1config()));
            }),
      ),
    );
  }
}
