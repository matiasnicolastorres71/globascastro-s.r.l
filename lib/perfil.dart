import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/main.dart';

import 'flexibleappbar2.dart';
import 'navegacion.dart';

class Perfil extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.lightBlue[700],
        accentColor: Color.fromRGBO(220, 140, 130, 1),
      ),
      home: Scaffold(
        backgroundColor: Colors.blue[100],
        bottomNavigationBar: Navegacion(),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              title: Text("METALURGICA M&V"),
              pinned: true,
              expandedHeight: 210.0,
              flexibleSpace: FlexibleSpaceBar(
                background: FlexibleAppbar2(),
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(<Widget>[]),
            )
          ],
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.blue,
          elevation: 0,
          highlightElevation: 0,
          child: Icon(CupertinoIcons.back),
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => App()));
          },
        ),
      ),
    );
  }
}
