import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/drawe.dart';
import 'package:flutter_application_1/navegacion.dart';

import 'card1.dart';
import 'card2.dart';
import 'card3.dart';
import 'flexibleapp.dart';
import "login_page.dart";
import 'drawe.dart';

void main() {
  //hola
  runApp(App());
}

class App extends StatelessWidget {
  //hola
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.lightBlue[700],
        accentColor: Color.fromRGBO(220, 140, 130, 1),
      ),
      home: Scaffold(
        backgroundColor: Colors.blue[100],
        drawer: MainDrawer(),
        bottomNavigationBar: Navegacion(),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              title: Text("METALURGICA M&V"),
              pinned: true,
              expandedHeight: 210.0,
              flexibleSpace: FlexibleSpaceBar(
                background: FlexibleAppbar(),
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(<Widget>[
                Card1(),
                Card2(),
                Card3(),
                Card1(),
                Card2(),
                Card3(),
                Card1(),
                Card2(),
              ]),
            )
          ],
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.blue,
          elevation: 0,
          highlightElevation: 0,
          child: Icon(CupertinoIcons.qrcode),
          onPressed: () {},
        ),
      ),
      initialRoute: LoginPage.id,
      routes: {LoginPage.id: (context) => LoginPage()},
    );
  }
}
