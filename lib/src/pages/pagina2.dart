import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/imagesCards/imagesCardsHerreria.dart';
import 'package:flutter_application_1/navegacion.dart';

class Pagina2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.lightBlue[700],
        accentColor: Color.fromRGBO(220, 140, 130, 1),
      ),
      home: Scaffold(
        backgroundColor: Colors.white,
        bottomNavigationBar: Navegacion(),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              title: Text("Trabajos de Herreria"),
              pinned: true,
              expandedHeight: 210.0,
              flexibleSpace: FlexibleSpaceBar(),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                <Widget>[
                  Imgcard2(),
                ],
              ),
            )
          ],
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.blue,
          elevation: 0,
          highlightElevation: 0,
          child: Icon(CupertinoIcons.back),
          onPressed: () {},
        ),
      ),
    );
  }
}
