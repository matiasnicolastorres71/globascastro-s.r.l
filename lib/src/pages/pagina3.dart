import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/navegacion.dart';

class Pagina3 extends StatelessWidget {
  get builder => null;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.lightBlue[700],
        accentColor: Color.fromRGBO(220, 140, 130, 1),
      ),
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            backgroundColor: Colors.blue,
            title: Center(
              child: Text(
                "Quienes Somos",
              ),
            )),
        bottomNavigationBar: Navegacion(),
        body: Center(
          child: Text(
            "METALURGICA M&V",
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.normal,
              fontSize: 20,
            ),
          ),
        ),
      ),
    );
  }
}
