import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/imagesCards/imagesCardsLoneria.dart';
import 'package:flutter_application_1/navegacion.dart';

class Pagina1 extends StatelessWidget {
  get builder => null;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.lightBlue[700],
        accentColor: Color.fromRGBO(220, 140, 130, 1),
      ),
      home: Scaffold(
          backgroundColor: Colors.blue[100],
          bottomNavigationBar: Navegacion(),
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                title: Text("Trabajos de Loneria"),
                pinned: true,
                expandedHeight: 210.0,
                flexibleSpace: FlexibleSpaceBar(),
              ),
              SliverList(
                delegate: SliverChildListDelegate(<Widget>[
                  Imgcard1(),
                ]),
              )
            ],
          )),
    );
  }
}
