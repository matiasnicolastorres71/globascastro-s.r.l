import 'package:flutter/material.dart';
import 'package:flutter_application_1/config.dart';
import 'package:flutter_application_1/login_page.dart';
import 'package:flutter_application_1/perfil.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(20),
            color: Colors.blue,
            child: Center(
              child: Column(
                children: <Widget>[
                  Container(
                    width: 100,
                    height: 100,
                    margin: EdgeInsets.only(
                      top: 30,
                      bottom: 10,
                    ),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: AssetImage("assets/Dario.jpg"),
                          fit: BoxFit.fill),
                    ),
                  ),
                  Text(
                    "Matias Torres",
                    style: TextStyle(fontSize: 22, color: Colors.white),
                  ),
                  Text(
                    "matiasnicolastorres71@gmail.com",
                    style: TextStyle(color: Colors.white),
                  )
                ],
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text("Perfil",
                style: TextStyle(
                  fontSize: 18,
                )),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => Perfil(),
                ),
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text("Configuracion",
                style: TextStyle(
                  fontSize: 18,
                )),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => Configuracion()));
            },
          ),
          ListTile(
            leading: Icon(Icons.arrow_back),
            title: Text("Cerrar Sesion",
                style: TextStyle(
                  fontSize: 18,
                )),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => LoginPage()));
            },
          )
        ],
      ),
    );
  }
}
