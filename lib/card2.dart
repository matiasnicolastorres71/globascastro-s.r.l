import 'package:flutter/material.dart';
import 'package:flutter_application_1/src/pages/pagina2.dart';

class Card2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        shadowColor: Colors.black,
        color: Colors.white,
        child: ListTile(
          title: Text(
            "Trabajos de Herreria",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          subtitle: Text(
            "Para ordenar un trabajo de loneria click aqui",
            style: TextStyle(color: Colors.black),
          ),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => Pagina2()));
          },
        ),
      ),
    );
  }
}
