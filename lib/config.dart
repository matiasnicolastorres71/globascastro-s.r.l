import 'package:flutter/material.dart';
import 'package:flutter_application_1/configuracion/cardConfig1.dart';

class Configuracion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text("Configuracion"),
          ),
        ),
        body: CustomScrollView(
          slivers: <Widget>[
            Cardconfig1(),
          ],
        ),
      ),
    );
  }
}
