import 'package:flutter/material.dart';
import 'package:flutter_application_1/src/pages/pagina1.dart';

class Card1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        shadowColor: Colors.black87,
        color: Colors.white,
        child: ListTile(
            title: Text(
              "Trabajos de Loneria",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              "Para ordenar un trabajo de loneria click aqui",
              style: TextStyle(color: Colors.black),
            ),
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Pagina1()));
            }),
      ),
    );
  }
}
